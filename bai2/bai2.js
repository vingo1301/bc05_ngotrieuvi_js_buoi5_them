function tinhTienCap(){
    var maKH = document.getElementById("txt-ma-kh").value;
    var loaiKH = document.getElementById("txt-loai-kh").value *1;
    var soKenh = document.getElementById("txt-so-kenh").value *1;
    var soKetNoi = document.getElementById("txt-so-ket-noi").value *1;
    var giaTien = 0;
    if (loaiKH == 0){
        giaTien = tinhTienCapNhaDan(soKenh);
        document.getElementById("result").innerHTML = `Tiền cáp của KH ${maKH} là ${giaTien}$`;
    }
    else{
        giaTien = tinhTienCapDoanhNghiep(soKenh,soKetNoi);
        document.getElementById("result").innerHTML = `Tiền cáp của KH ${maKH} là ${giaTien}$`;
    }
}
function tinhTienCapDoanhNghiep(soKenh,soKetNoi){
    if (soKetNoi <= 10){
        var giaTien = 15 + 75 + 50*soKenh;
    }
    else{
        var giaTien = 15 + 75 + (soKetNoi - 10)*5 + 50*soKenh;
    }
    return giaTien;
    
}
function tinhTienCapNhaDan(soKenh){
    var giaTien = 4.5 + 20.5 +7.5*soKenh;
    return giaTien;

}
function hienThi(){
    var giaTri = document.getElementById("txt-loai-kh").value*1;
    if (giaTri == 0){
        document.getElementById("txt-so-ket-noi").style.display = "none";
    } else{
        document.getElementById("txt-so-ket-noi").style.display = "block";
    }
}