function ketQua(){
    var name = document.getElementById("txt-ten").value;
    var thuNhap = document.getElementById("txt-thu-nhap").value*1;
    var nguoi = document.getElementById("txt-nguoi").value*1;
    var tienThue = new Intl.NumberFormat().format(tinhThue(thuNhap,nguoi));
    document.getElementById("result").innerHTML = `Tiền Thuế của ${name} là ${tienThue} VND`;
}
function tinhThue(thuNhap,nguoi){
    var tienThue = 0;
    if (thuNhap <= 60000000){
        tienThue = (thuNhap - 4000000 - nguoi*1600000)*5/100;
    }
    else if(thuNhap <= 120000000){
        tienThue = (thuNhap - 4000000 - nguoi*1600000)*10/100;
    } else if(thuNhap <= 210000000){
        tienThue = (thuNhap - 4000000 - nguoi*1600000)*15/100;
    } else if(thuNhap <= 384000000){
        tienThue = (thuNhap - 4000000 - nguoi*1600000)*20/100;
    } else if(thuNhap <= 624000000){
        tienThue = (thuNhap - 4000000 - nguoi*1600000)*25/100;
    } else if(thuNhap <= 960000000){
        tienThue = (thuNhap - 4000000 - nguoi*1600000)*30/100;
    }
    else{
        tienThue = (thuNhap - 4000000 - nguoi*1600000)*35/100;
    }
    if(tienThue <= 0){
        tienThue = 0;
    }
    return tienThue;
}